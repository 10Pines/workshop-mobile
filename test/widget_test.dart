import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:wordle/constantes/colores.dart';
import 'package:wordle/constantes/mensajes.dart';
import 'package:wordle/constantes/reglas.dart';
import 'package:wordle/main.dart';
import 'package:wordle/modelo/diccionario.dart';
import 'package:wordle/screens/home_page.dart';

void main() {
  late DiccionarioSecuencial diccionario;
  late HomePage homePage;
  late String palabraActual;

  setUp(() {
    diccionario = DiccionarioSecuencial(palabras: [
      "HOLAS",
      "GATOS",
      "PELOS",
      "HORNO",
      "BEBEN",
      "OLASH",
      "HOAAL",
      "HAAOA"
    ], palabraSeleccionada: 0);
    palabraActual = diccionario.palabraActual();

    homePage = HomePage(diccionario: diccionario);
  });

  testWidgets(
      'Cuando le defino un titulo a la HomePage este lo muestra en pantalla',
      (WidgetTester tester) async {
    await tester.pumpWidget(const MyApp());

    expect(find.text('Wordle'), findsOneWidget);
  });

  testWidgets(
      'Al iniciar el tablero tiene una cantidad de campos para palabras',
      (WidgetTester tester) async {
    await tester.pumpWidget(_wrapWidget(homePage));

    expect(_buscarFilaConCampos(), findsNWidgets(5));
  });

  testWidgets('Para ingresar una palabra existen 5 campos',
      (WidgetTester tester) async {
    await tester.pumpWidget(_wrapWidget(homePage));

    expect(_buscarCamposParaUnaPalabra(), findsNWidgets(5));
  });

  testWidgets(
      'Cuando presiono una tecla se escribe esa letra en el campo de texto',
      (WidgetTester tester) async {
    await tester.pumpWidget(_wrapWidget(homePage));

    await _introducirLetraConElTeclado(tester, "H");

    expect(_buscarCampoConLetra("H"), findsOneWidget);
  });

  testWidgets(
      'Al iniciar la aplicación, el foco se encuentra en el primer campo del primer intento',
      (WidgetTester tester) async {
    await tester.pumpWidget(_wrapWidget(homePage));
    const indiceCampo = 0;
    const numeroIntento = 0;

    final primerCampo = _obtenerCampoPorKey(tester,
        indiceDeCampoActual: indiceCampo, indiceDeFila: numeroIntento);

    expect(primerCampo.focusNode?.hasFocus, true);
  });

  testWidgets(
      'Al ingresar una letra en un campo, se pasa el foco al siguiente campo',
      (WidgetTester tester) async {
    await tester.pumpWidget(_wrapWidget(homePage));
    const indexDeCampoActual = 0;

    await _introducirLetraConElTeclado(tester, "H");

    final siguienteCampo = _obtenerCampoPorKey(tester,
        indiceDeCampoActual: indexDeCampoActual + 1, indiceDeFila: 0);
    final campoInicial = _obtenerCampoPorKey(tester,
        indiceDeCampoActual: indexDeCampoActual, indiceDeFila: 0);
    expect(siguienteCampo.focusNode?.hasFocus, true);
    expect(campoInicial.focusNode?.hasFocus, false);
  });

  testWidgets(
      'Al oprimir la tecla de retroceso en un campo con letra existente, la borra',
      (WidgetTester tester) async {
    await tester.pumpWidget(_wrapWidget(homePage));
    const indiceCampo = 0;
    const numeroIntento = 0;
    await _introducirLetraConElTeclado(tester, "H");
    await _tapEn(tester,
        indiceCampo: indiceCampo, numeroIntento: numeroIntento);

    await _simularTeclaPresionada(LogicalKeyboardKey.backspace);

    final campo = _obtenerCampoPorKey(tester,
        indiceDeCampoActual: indiceCampo, indiceDeFila: numeroIntento);
    _chequearQueLosCamposEstenVacios([campo]);
  });

  testWidgets(
      'Al oprimir la tecla de retroceso en un campo vacio, mueve el foco al campo previo en caso que exista',
      (WidgetTester tester) async {
    await tester.pumpWidget(_wrapWidget(homePage));
    const indiceCampo = 0;
    const numeroIntento = 0;
    await _introducirLetraConElTeclado(tester, "H");

    await _simularTeclaPresionada(LogicalKeyboardKey.backspace);

    final campoConLetra = _obtenerCampoPorKey(tester,
        indiceDeCampoActual: indiceCampo, indiceDeFila: numeroIntento);
    final siguienteCampo = _obtenerCampoPorKey(tester,
        indiceDeCampoActual: indiceCampo + 1, indiceDeFila: numeroIntento);
    expect(campoConLetra.focusNode?.hasFocus, true);
    expect(siguienteCampo.focusNode?.hasFocus, false);
  });

  testWidgets('Existe el boton para enviar la palabra',
          (WidgetTester tester) async {
        await tester.pumpWidget(_wrapWidget(homePage));

        expect(find.text('Enviar'), findsOneWidget);
      });

  testWidgets(
      'Al ingresar una palabra que es incorrecta con intentos pendientes, el foco se mueve al primer campo del próximo intento',
      (WidgetTester tester) async {
    await tester.pumpWidget(_wrapWidget(homePage));
    const numeroIntento = 0;
    const proximoIntento = numeroIntento + 1;
    const indiceCampo = 0;
    await _introducirPalabraConElTeclado(tester, "HAAOA");

    await _presionarElBotonEnviar(tester);

    final campoConFoco = _obtenerCampoPorKey(tester,
        indiceDeCampoActual: indiceCampo, indiceDeFila: proximoIntento);
    expect(campoConFoco.focusNode?.hasFocus, true);
  });

  testWidgets(
      'Al tocar filas de palabras que no pertenezcan al intento actual, no obtienen foco',
      (WidgetTester tester) async {
    await tester.pumpWidget(_wrapWidget(homePage));
    const indiceCampoConFoco = 0;
    const numeroIntentoActual = 0;
    const indiceCampoTocado = 3;
    const intentoNoActual = 2;

    await _tapEn(tester,
        indiceCampo: indiceCampoTocado, numeroIntento: intentoNoActual);

    final campoConFoco = _obtenerCampoPorKey(tester,
        indiceDeCampoActual: indiceCampoConFoco,
        indiceDeFila: numeroIntentoActual);
    final campoTocado = _obtenerCampoPorKey(tester,
        indiceDeCampoActual: indiceCampoTocado, indiceDeFila: intentoNoActual);

    expect(campoConFoco.focusNode?.hasFocus, true);
    expect(campoTocado.focusNode?.hasFocus, false);
  });

  testWidgets(
      'Cuando escribo en una celda con una letra, se reemplaza la letra existente',
          (WidgetTester tester) async {
        await tester.pumpWidget(_wrapWidget(homePage));

        await _introducirLetraConElTeclado(tester, "C");
        await _presionarElBotonBorrar(tester);
        await _introducirLetraConElTeclado(tester, "H");

        expect(_buscarCampoConLetra("H"), findsOneWidget);
        expect(_buscarCampoConLetra("C"), findsNothing);
      });

  testWidgets(
      'Al presionar el boton Enviar teniendo la palabra del dia adivinada, muestra una toast notification y se puede reiniciar la partida',
          (WidgetTester tester) async {
        await tester.pumpWidget(_wrapWidget(homePage));
        await _introducirPalabraConElTeclado(tester, palabraActual);

        await _presionarElBotonEnviar(tester);

        _validarMensajeDeSnackBar(Mensaje.exito);
        expect(find.text('Reiniciar'), findsOneWidget);
      });

  testWidgets(
      'Al presionar el boton Enviar sin completar todas las celdas, se muestra un mensaje',
          (WidgetTester tester) async {
        await tester.pumpWidget(_wrapWidget(homePage));

        await _presionarElBotonEnviar(tester);

        _validarMensajeDeSnackBar(Mensaje.palabraIncompleta);
      });

  testWidgets(
      'Al realizar todos los intentos disponibles con palabras incorrectas, se muestra un mensaje de derrota y se puede reiniciar la partida',
          (WidgetTester tester) async {
        await tester.pumpWidget(_wrapWidget(homePage));
        await _realizarIntentos(tester, 5);

        _validarMensajeDeSnackBar(Mensaje.derrota(palabraActual));
        expect(find.text('Reiniciar'), findsOneWidget);
      });

  testWidgets(
      'Al presionar el boton Enviar con una palabra que no se encuentra en el diccionario, se muestra un mensaje',
          (WidgetTester tester) async {
        await tester.pumpWidget(_wrapWidget(homePage));

        await _introducirPalabraConElTeclado(tester, "MONTO");

        await _presionarElBotonEnviar(tester);

        _validarMensajeDeSnackBar(Mensaje.noSeEncuentraPalabra);
      });

  testWidgets(
      'Al presionar el boton de reiniciar partida se vacian los campos y cambia la palabra a adivinar',
          (WidgetTester tester) async {
        await tester.pumpWidget(_wrapWidget(homePage));
        final palabraAnterior = palabraActual;
        await _introducirPalabraConElTeclado(tester, palabraActual);

        await _presionarElBotonEnviar(tester);
        _validarMensajeDeSnackBar(Mensaje.exito);
        expect(find.text('Reiniciar'), findsOneWidget);

        await tester.pump();
        await _presionarElBotonReiniciar(tester);

        final campos = _obtenerTodosLosCampos(tester, 0);

        _chequearQueLosCamposEstenVacios(campos);
        expect(palabraAnterior, isNot(equals(diccionario.palabraActual())));
      });

  testWidgets(
      'Al ingresar una palabra, se pintan de gris las letras que no estan contenidas en la palabra',
      (WidgetTester tester) async {
    await tester.pumpWidget(_wrapWidget(homePage));

    await _introducirPalabraConElTeclado(tester, "BEBEN");
    await _presionarElBotonEnviar(tester);

    final List<TextField> campos = _obtenerCamposDeUnaPalabra(tester);
    _chequearQueEstenPintadasDelColor(campos, Colores.gris);
  });

  testWidgets(
      'Al ingresar una palabra, se pintan de gris cuando la letra se encuentra adivinada antes de la celda a adivinar',
      (WidgetTester tester) async {
    await tester.pumpWidget(_wrapWidget(homePage));
    const numeroDeIntento = 0;

    await _introducirPalabraConElTeclado(tester, "HORNO");
    await _presionarElBotonEnviar(tester);

    final camposDeGris = _obtenerCamposDeUnaFilaSegunPosicion(tester,
        indicesDeCampo: [4], indiceDeFila: numeroDeIntento);
    final camposDeVerde = _obtenerCamposDeUnaFilaSegunPosicion(tester,
        indicesDeCampo: [1], indiceDeFila: numeroDeIntento);

    _chequearQueEstenPintadasDelColor(camposDeGris, Colores.gris);
    _chequearQueEstenPintadasDelColor(camposDeVerde, Colores.verde);
  });

  testWidgets(
      'Al ingresar una palabra, se pintan de verde las letras que se encuentran en la palabra y en la posición correcta',
      (WidgetTester tester) async {
    await tester.pumpWidget(_wrapWidget(homePage));

    await _introducirPalabraConElTeclado(tester, "HOLAS");
    await _presionarElBotonEnviar(tester);

    final campos = _obtenerCamposDeUnaPalabra(tester);

    _chequearQueEstenPintadasDelColor(campos, Colores.verde);
  });

  testWidgets(
      'Al ingresar una palabra, se pintan de amarillo las letras que estan contenidas en la palabra pero en la posicion incorrecta',
      (WidgetTester tester) async {
    await tester.pumpWidget(_wrapWidget(homePage));

    await _introducirPalabraConElTeclado(tester, "OLASH");
    await _presionarElBotonEnviar(tester);

    final campos = _obtenerCamposDeUnaPalabra(tester);

    _chequearQueEstenPintadasDelColor(campos, Colores.amarillo);
  });

  testWidgets(
      'Al ingresar una palabra, se pintan de gris cuando la letra se encuentra adivinada despues de la celda a adivinar',
      (WidgetTester tester) async {
    await tester.pumpWidget(_wrapWidget(homePage));
    const numeroDeIntento = 0;
    await _introducirPalabraConElTeclado(tester, "HOAAL");
    await _presionarElBotonEnviar(tester);

    final camposDeGris = _obtenerCamposDeUnaFilaSegunPosicion(tester,
        indicesDeCampo: [2], indiceDeFila: numeroDeIntento);
    final camposDeVerde = _obtenerCamposDeUnaFilaSegunPosicion(tester,
        indicesDeCampo: [3], indiceDeFila: numeroDeIntento);

    _chequearQueEstenPintadasDelColor(camposDeGris, Colores.gris);
    _chequearQueEstenPintadasDelColor(camposDeVerde, Colores.verde);
  });

  testWidgets(
      'Al ingresar una palabra, se pintan de gris cuando la letra ya fue encontrada y pintada de amarillo',
      (WidgetTester tester) async {
    await tester.pumpWidget(_wrapWidget(homePage));

    await _introducirPalabraConElTeclado(tester, "HAAOA");
    await _presionarElBotonEnviar(tester);

    final camposDeGris = _obtenerCamposDeUnaFilaSegunPosicion(tester,
        indicesDeCampo: [2, 4], indiceDeFila: 0);
    final camposDeAmarillo = _obtenerCamposDeUnaFilaSegunPosicion(tester,
        indicesDeCampo: [1], indiceDeFila: 0);

    _chequearQueEstenPintadasDelColor(camposDeGris, Colores.gris);
    _chequearQueEstenPintadasDelColor(camposDeAmarillo, Colores.amarillo);
  });

  testWidgets(
      'Al ingresar una palabra se pintan las teclas del color que corresponde',
      (WidgetTester tester) async {
    await tester.pumpWidget(_wrapWidget(homePage));

    await _introducirPalabraConElTeclado(tester, "GATOS");
    await _presionarElBotonEnviar(tester);

    final teclasDeGris = _obtenerTeclas(tester, ["G", "T"]);
    final teclasDeAmarillo = _obtenerTeclas(tester, ["A", "O"]);
    final teclasDeVerde = _obtenerTeclas(tester, ["S"]);

    _chequearQueLasTeclasEstenPintadas(teclasDeGris, Colores.gris);
    _chequearQueLasTeclasEstenPintadas(teclasDeAmarillo, Colores.amarillo);
    _chequearQueLasTeclasEstenPintadas(teclasDeVerde, Colores.verde);
  });

  testWidgets(
      'Cuando una tecla es pintada de verde no se cambia de color en otros intentos',
      (WidgetTester tester) async {
    await tester.pumpWidget(_wrapWidget(homePage));

    await _introducirPalabraConElTeclado(tester, "GATOS");
    await _presionarElBotonEnviar(tester);
    await _introducirPalabraConElTeclado(tester, "OLASH");
    await _presionarElBotonEnviar(tester);

    final teclasDeVerde = _obtenerTeclas(tester, ["S"]);

    _chequearQueLasTeclasEstenPintadas(teclasDeVerde, Colores.verde);
  });

  testWidgets(
      'Cuando una tecla es pintada de amarillo puede cambiar a verde si se adivina la posicion correcta',
      (WidgetTester tester) async {
    await tester.pumpWidget(_wrapWidget(homePage));

    await _introducirPalabraConElTeclado(tester, "OLASH");
    await _presionarElBotonEnviar(tester);

    final teclasDeAmarillo = _obtenerTeclas(tester, ["S"]);

    _chequearQueLasTeclasEstenPintadas(teclasDeAmarillo, Colores.amarillo);

    await _introducirPalabraConElTeclado(tester, "GATOS");
    await _presionarElBotonEnviar(tester);

    final teclasDeVerde = _obtenerTeclas(tester, ["S"]);

    _chequearQueLasTeclasEstenPintadas(teclasDeVerde, Colores.verde);
  });
}

Future<void> _tapEn(WidgetTester tester,
    {required int indiceCampo, required int numeroIntento}) async {
  await tester.tap(find.byKey(Reglas.crearKey(
      focoAutomatico: indiceCampo == 0,
      indiceDeLetra: indiceCampo,
      indiceDeFila: numeroIntento)));
}

List<TextField> _obtenerCamposDeUnaPalabra(WidgetTester tester) {
  return tester.widgetList<TextField>(_buscarCamposParaUnaPalabra()).toList();
}

Future<void> _realizarIntentos(
    WidgetTester tester, int cantidadDeIntentos) async {
  for (int intentoActual = 0;
      intentoActual < cantidadDeIntentos;
      intentoActual++) {
    await _introducirPalabraConElTeclado(tester, "GATOS");
    await _presionarElBotonEnviar(tester);
  }
}

void _chequearQueLosCamposEstenVacios(List<TextField> campos) {
  for (var campo in campos) {
    expect(campo.controller?.text, "");
  }
}

List<TextField> _obtenerTodosLosCampos(WidgetTester tester, int indiceDeFila) =>
    _obtenerCamposDeUnaFilaSegunPosicion(tester,
        indicesDeCampo: [0, 1, 2, 3, 4], indiceDeFila: indiceDeFila);

void _chequearQueEstenPintadasDelColor(List<TextField> campos, Color color) {
  for (var campo in campos) {
    expect(campo.decoration?.filled, true);
    expect(campo.decoration?.fillColor, color);
  }
}

List<TextField> _obtenerCamposDeUnaFilaSegunPosicion(WidgetTester tester,
    {required List<int> indicesDeCampo, required int indiceDeFila}) {
  return indicesDeCampo
      .map((index) => _obtenerCampoPorKey(tester,
          indiceDeCampoActual: index, indiceDeFila: indiceDeFila))
      .toList();
}

Finder _buscarCamposParaUnaPalabra() {
  return find.descendant(
      of: find.byType(Row).first, matching: find.byType(TextField));
}

Finder _buscarFilaConCampos() {
  return find.byWidgetPredicate(
      (widget) => widget is Row && widget.children.length == 5);
}

Future<bool> _simularTeclaPresionada(LogicalKeyboardKey key) async {
  final bool handled = await KeyEventSimulator.simulateKeyDownEvent(key);
  final ServicesBinding binding = ServicesBinding.instance;
  if (!handled && binding is TestWidgetsFlutterBinding) {
    await binding.testTextInput.handleKeyDownEvent(key);
  }
  return handled;
}

TextField _obtenerCampoPorKey(WidgetTester tester,
    {required int indiceDeCampoActual, required int indiceDeFila}) {
  return tester.widget(find.byKey(Reglas.crearKey(
      focoAutomatico: indiceDeCampoActual == 0,
      indiceDeFila: indiceDeFila,
      indiceDeLetra: indiceDeCampoActual)));
}

void _validarMensajeDeSnackBar(String mensaje) {
  expect(find.byType(SnackBar), findsOneWidget);
  expect(find.text(mensaje), findsOneWidget);
}

Future<void> _presionarElBotonEnviar(WidgetTester tester) async {
  await tester.tap(find.text('Enviar'));
  await tester.pump();
}

Future<void> _presionarElBotonReiniciar(WidgetTester tester) async {
  await tester.tap(find.text('Reiniciar'));
  await tester.pump();
}

Future<void> _presionarElBotonBorrar(WidgetTester tester) async {
  await tester.tap(find.byIcon(Icons.backspace));
  await tester.pump();
}

Widget _wrapWidget(Widget child) {
  return MaterialApp(
    home: Scaffold(
      body: child,
    ),
  );
}

List<ElevatedButton> _obtenerTeclas(WidgetTester tester, List<String> letras) {
  return letras
      .map<ElevatedButton>((letra) => tester.widget(_buscarTecla(letra)))
      .toList();
}

void _chequearQueLasTeclasEstenPintadas(
    List<ElevatedButton> teclas, Color color) {
  for (var tecla in teclas) {
    expect(tecla.style?.backgroundColor?.resolve(MaterialState.values.toSet()),
        color);
  }
}

Future<void> _introducirPalabraConElTeclado(
    WidgetTester tester, String palabraAChequear) async {
  final letrasDePalabraDelDia = palabraAChequear.split("");

  for (var letra in letrasDePalabraDelDia) {
    await _introducirLetraConElTeclado(tester, letra);
  }
}

Future<void> _introducirLetraConElTeclado(
    WidgetTester tester, String letra) async {
  await tester.tap(_buscarTecla(letra));
  await tester.pump();
}

Finder _buscarTecla(String letra) {
  return find.ancestor(
      of: find.text(letra), matching: find.byType(ElevatedButton));
}

Finder _buscarCampoConLetra(String letra) =>
    find.ancestor(of: find.text(letra), matching: find.byType(TextField));
