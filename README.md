# wordle
Ejercicio para el workshop mobile de la 10pinesconf 2023.

# Reglas
Wordle es un juego de palabras donde tenes que adivinar cual es la palabra oculta.
Para ello contamos con una serie de 5 intentos para ingresar una palabra de 5 letras.
Si se adivina la palabra antes de utilizar todos los intentos entonces se gana el juego.
Si dentro de los 5 intentos no se logra adivinar la palabra se pierde el juego.
Para saber si vas por buen camino, el juego te da una serie de pistas pintando las letras de la palabra ingresada y el teclado con colores.
Cada color tiene su significado:

- Si la letra aparece en verde, es porque le has acertado y está en la palabra, y también está en la casilla correcta de la palabra.
- Si la letra aparece en amarillo, es porque está en la palabra, pero no está en la casilla correcta.
- Si la letra aparece en gris es porque no has acertado, y no está en la palabra que tienes que adivinar.

Por lo tanto, si toda la palabra está pintada de verde significa que has acertado la palabra.

Vas a poder ingresar las letras de las palabras en el orden que quieras y una vez que estés seguro, se pulsa el botón enviar para que compruebe la palabra ingresada.
Además el juego cuenta con un diccionario de palabras, es decir, que si ingresas una palabra que no se encuentra en el diccionario no te va a dejar validarla.
Una vez terminado el juego podrás comenzar un juego nuevo con una palabra distinta a adivinar.

![](lib/imagenes/teclado.png)

##Funcionalidades a implementar:

- Poder ingresar una palabra de 5 letras con ayuda del teclado. Una letra por cada celda.
- Chequear que cumple las condiciones para validar la palabra al presionar el boton enviar.
  - Mostrar el mensaje "La palabra no se encuentra en el diccionario" cuando la palabra no se encuentra en el diccionario.
  - Mostrar el mensaje "No hay suficientes letras" cuando hay al menos una celda incompleta.
- Poder validar la palabra al presionar el botón enviar.
  - Mostrar el mensaje "Adivinaste la palabra!" cuando la palabra ingresada es la correcta.
  - Mostrar el mensaje "La palabra correcta era: GATOS" cuando no se adivina la palabra correcta en el último intento
- Pintar las celdas del color que corresponde.
  - Si se ingresa dos veces la misma letra pero solo se encuentra contenida una vez en la palabra. Entonces se pinta de color amarillo en la primera aparicion o de verde en la posicion correcta.
  - Ejemplos:
    - La palabra correcta es `HOLAS` y la palabra ingresada es `GATAS`. La `G`, la primera `A` y la `T` son grises y la segunda `A` y la `S` son verdes.
    - La palabra correcta es `GATOS` y la palabra ingresada es `ARMAS`. La `R`, `M` y la segunda `A` son grises, la `S` es verde y la primera `A` es amarillo.
- Hay 5 intentos para adivinar la palabra.
  - En caso de fallar los 5 intentos se pierde el juego mostrando el mensaje de derrota.
  - En caso de adivinar la palabra en uno de los intentos se gana el juego mostrando el mensaje de victoria.
- Cuando se termina el juego debe ser posible comenzar un juego nuevo. Para esto, se suplanta el botón de enviar por el de reiniciar.
  - Se deben limpiar los campos.
  - Se cambia la palabra a adivinar.
- Poder navegar entre los campos de manera sencilla.
  - Cuando válidas una palabra que se encuentra en el diccionario y es incorrecta, se hace foco en el primer campo de la siguiente fila.
  - Cuando inicias la aplicación, se hace foco en la primera celda.

## Authors and acknowledgment
10pines

