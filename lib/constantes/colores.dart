import 'package:flutter/material.dart';

//TODO: utilizar colores para pintar las celdas y las teclas
class Colores {
  static const Color gris = Color(0xFF757575);
  static const Color amarillo = Color(0xFFe4a91d);
  static const Color verde = Color(0xFF44a047);
}
