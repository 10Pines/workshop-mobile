//TODO: utilizar mensajes para dar feedback al usuario
class Mensaje {
  static const String exito = "Adivinaste la palabra!";
  static const String palabraIncompleta = "No hay suficientes letras";
  static const String noSeEncuentraPalabra =
      "La palabra no se encuentra en el diccionario";

  static String derrota(String palabraDelDia) =>
      "La palabra correcta era: $palabraDelDia";
}
