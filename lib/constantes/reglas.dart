import 'package:flutter/material.dart';

//TODO: utilizar reglas de juego y la Key para indentificar los TextFormField
class Reglas {
  static const int intentosPosibles = 5;
  static const int letrasPorPalabra = 5;

  static Key crearKey(
      {required bool focoAutomatico,
      required int indiceDeFila,
      required int indiceDeLetra}) {
    return Key("campo-para-letra-$focoAutomatico$indiceDeFila$indiceDeLetra");
  }
}
