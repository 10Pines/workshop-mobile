import 'package:flutter/material.dart';
import 'package:wordle/constantes/reglas.dart';

//TODO: utilizar para contruir un campo para una letra
///posicion: posicion en la que se encuentra en la fila que va del 0 al 4
///color: color con el que va a ser pintado. Amarillo, gris o verde
///controlDeTexto: necesario para manejar el texto que ingresa el jugador
///focoAutomatico: indica si comienza con foco al ser creado
///numeroDeFila: numero de la fila en la que se encuentra que va del 0 al 4 en caso de tener 5 intentos
///focusNode: necesario para manejar los cambios de foco en este campo
class CampoParaLetra extends StatelessWidget {
  final TextEditingController controlDeTexto;
  final int posicion;
  final Color? color;
  final bool focoAutomatico;
  final int numeroFila;
  final FocusNode focusNode;

  const CampoParaLetra({
    super.key,
    required this.controlDeTexto,
    required this.posicion,
    required this.numeroFila,
    required this.focusNode,
    this.focoAutomatico = false,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    final bool estaPintado = color != null;
    return Expanded(
        child: Padding(
      padding: const EdgeInsets.all(2.0),
      child: TextField(
        keyboardType: TextInputType.none,
        focusNode: focusNode,
        autofocus: focoAutomatico,
        key: Reglas.crearKey(
            focoAutomatico: focoAutomatico,
            indiceDeFila: numeroFila,
            indiceDeLetra: posicion),
        controller: controlDeTexto,
        style: Theme.of(context)
            .textTheme
            .titleLarge
            ?.copyWith(color: estaPintado ? Colors.white : Colors.black),
        textAlign: TextAlign.center,
        enableInteractiveSelection: false,
        decoration: InputDecoration(
          enabled: color == null,
          disabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: color ?? Colors.grey)),
          border: const OutlineInputBorder(),
          filled: estaPintado,
          fillColor: color,
        ),
      ),
    ));
  }
}
