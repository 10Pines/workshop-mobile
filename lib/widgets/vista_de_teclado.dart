import 'package:flutter/material.dart';
import 'package:wordle/modelo/informacion_de_tecla.dart';
import 'package:wordle/modelo/intento.dart';
import 'package:wordle/modelo/teclado.dart';


///Recibe el intento actual
///Para que muestre el boton de reiniciar, terminoElJuego debe estar en true
class VistaDeTeclado extends StatelessWidget {
  final bool terminoElJuego;
  final void Function() comenzarNuevoJuego;
  final void Function(BuildContext context) chequearPalabra;
  final Intento intento;
  final Teclado teclado;

  const VistaDeTeclado({
    required this.terminoElJuego,
    required this.comenzarNuevoJuego,
    required this.chequearPalabra,
    required this.intento,
    required this.teclado,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<Widget> ultimaFilaDelTeclado = [
      terminoElJuego
          ? ElevatedButton(
              onPressed: comenzarNuevoJuego, child: const Text("Reiniciar"))
          : ElevatedButton(
              onPressed: () => chequearPalabra(context),
              child: const Text("Enviar")),
    ];

    ultimaFilaDelTeclado.addAll(teclas.getRange(20, 27).map<Widget>((tecla) =>
        Tecla(intento: intento, letra: tecla.letra, color: tecla.color)));

    ultimaFilaDelTeclado.add(
      ElevatedButton(
          onPressed: () {
            intento.borrarLetra(context);
          },
          child: const Icon(
            Icons.backspace,
          )),
    );

    return FittedBox(
      child: Column(
        children: [
          Row(
            children: teclas
                .getRange(0, 10)
                .map((tecla) => Tecla(
                    intento: intento, letra: tecla.letra, color: tecla.color))
                .toList(),
          ),
          Row(
            children: teclas
                .getRange(10, 20)
                .map((tecla) => Tecla(
                    intento: intento, letra: tecla.letra, color: tecla.color))
                .toList(),
          ),
          Row(
            children: ultimaFilaDelTeclado,
          ),
        ],
      ),
    );
  }

  List<InformacionDeTecla> get teclas => teclado.teclas;
}

class Tecla extends StatelessWidget {
  final Intento intento;
  final String letra;
  final Color? color;

  const Tecla({
    required this.intento,
    required this.letra,
    this.color,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ButtonStyle(backgroundColor: MaterialStatePropertyAll(color)),
      onPressed: () {
        intento.escribirLetra(letra, context);
      },
      child: Text(
        letra,
        style: TextStyle(color: color != null ? Colors.white : Colors.black),
      ),
    );
  }
}
