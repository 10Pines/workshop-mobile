import 'package:flutter/material.dart';

//TODO: utilizar metodo para mostrar mensajes en pantalla
void mostrarMensaje(String mensaje, BuildContext context) {
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    content: Text(mensaje),
    showCloseIcon: true,
  ));
}