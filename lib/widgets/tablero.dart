import 'package:flutter/material.dart';
import 'package:wordle/modelo/intento.dart';

class Tablero extends StatelessWidget {
  final List<Intento> intentos;
  final int indiceIntentoActual;

  const Tablero({
    super.key,
    required this.intentos,
    required this.indiceIntentoActual,
  });

  @override
  Widget build(BuildContext context) {
    //TODO Por cada [Intento] crear cinco [CampoParaLetra] que representan la palabra.
    ///  Cada [Intento] almacena [CampoDeLetra] con información que permite crear
    ///  cada widget [CampoParaLetra] (controlDeText, focusNode, color, posicion).
    ///  [Intento.numero] se corresponde con [CampoParaLetra.numeroFila].
    ///  La lista de widgets puede ser creada con [List.generate].
    return const Placeholder(
      child: Text("Tablero"),
    );
  }
}
