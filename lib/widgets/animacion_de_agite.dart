import 'dart:math';

import 'package:flutter/material.dart';

///Animacion que hace vibrar el widget que contiene como hijo
///Se puede utilizar en la fila para ingresar una palabra cuando la palabra no esta contenida en el diccionario
///Para hacerlo funcionar hay que pasarle el controller del Intento y utilizar el metodo animar de la clase Intento
class AnimacionDeAgite extends StatefulWidget {
  final Widget child;
  final AnimationController animationController;

  const AnimacionDeAgite({
    required this.child,
    required this.animationController,
    Key? key,
  }) : super(key: key);

  @override
  State<AnimacionDeAgite> createState() => AnimacionDeAgiteState();
}

class AnimacionDeAgiteState extends State<AnimacionDeAgite>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {
    widget.animationController.addStatusListener(_actualizarEstado);
    super.initState();
  }

  @override
  void dispose() {
    widget.animationController.removeStatusListener(_actualizarEstado);
    super.dispose();
  }

  void _actualizarEstado(AnimationStatus estado) {
    if (estado == AnimationStatus.completed) {
      widget.animationController.reset();
    }
  }

  @override
  Widget build(BuildContext context) {
    const repeticiones = 3;
    const escalado = 10;

    return AnimatedBuilder(
      animation: widget.animationController,
      child: widget.child,
      builder: (context, child) {
        final traslado =
            sin(repeticiones * 2 * pi * widget.animationController.value);

        return Transform.translate(
          offset: Offset(traslado * escalado, 0),
          child: child,
        );
      },
    );
  }
}
