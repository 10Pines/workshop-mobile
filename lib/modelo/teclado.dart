import 'package:flutter/material.dart';
import 'package:wordle/modelo/informacion_de_tecla.dart';

class Teclado {

  final List<InformacionDeTecla> teclas = "QWERTYUIOPASDFGHJKLÑZXCVBNM"
      .split("")
      .map((letra) => InformacionDeTecla(letra: letra))
      .toList();

  ///TODO: utilizar metodo para pintar la tecla con esa letra del color que se le indique.
  void pintar(String letra, Color color) {
    for (var tecla in teclas) {
      if (tecla.letra == letra && !tecla.estaPintadaDeVerde()) {
        tecla.color = color;
      }
    }
  }
}