import 'package:flutter/material.dart';
import 'package:wordle/constantes/reglas.dart';
import 'package:wordle/modelo/campo_de_letra.dart';
import 'package:wordle/modelo/teclado.dart';

///Un intento representa un ingreso de palabra la cual tiene 5 letras
///En total vamos a tener 5 intentos en el juego
///Se encarga de manejar los CamposDeLetra y la animacion
///numero: en el numero del intento que va de 0 a 4
class Intento {
  final int numero;
  final AnimationController? controlDeAnimacion;
  List<CampoDeLetra> camposDeLetra;

  Intento(this.numero, {this.controlDeAnimacion})
      : camposDeLetra = List.generate(Reglas.letrasPorPalabra,
          (indice) => CampoDeLetra(posicion: indice));

  void dispose() {
    for (var campoDeLetra in camposDeLetra) {
      campoDeLetra.dispose();
    }
    controlDeAnimacion?.dispose();
  }

  bool faltaUnaLetra() =>
      camposDeLetra.any((controller) => controller.controlDeTexto.text.isEmpty);

  bool noExistePalabraEnElDiccionario(diccionario) =>
      !diccionario.existePalabraEnElDiccionario(palabraAChequear());

  String palabraAChequear() =>
      camposDeLetra
          .map((campoDeTexto) => campoDeTexto.controlDeTexto.text)
          .join();

  bool esLaPalabraCorrecta(String palabraDelDia) {
    return palabraAChequear() == palabraDelDia;
  }

  void pintarCeldas(String palabraDelDia, Teclado teclado) {
    //TODO Implementar
    /// Pintar de amarillo, verde o gris cada letra del teclado según corresponda.
    /// Los métodos [CampoDeLetra.pintarVerde] y [CampoDeLetra.pintarAmarilloOGris]
    /// ayudan a determinar que color corresponde.
  }

  void escribirLetra(String letra, BuildContext context) {
    _obtenerCampoConFocus()?.escribirLetra(letra, context);
  }

  CampoDeLetra? _obtenerCampoConFocus() {
    if (camposDeLetra.any((campoDeLetra) => campoDeLetra.tieneFocus())) {
      return camposDeLetra.firstWhere(
            (campoDeLetra) => campoDeLetra.tieneFocus(),
      );
    }
    return null;
  }

  void borrarLetra(BuildContext context) {
    _obtenerCampoConFocus()?.borrarLetra(context);
  }

  ///Metodo para animar las celdas de una palabra en caso de utilizar la animacion de agite
  void animar() {
    controlDeAnimacion?.forward();
  }
}
