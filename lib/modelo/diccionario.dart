import 'dart:math';

abstract class Diccionario {
  final List<String> palabras;

  const Diccionario({required this.palabras});

  //TODO: utilizar metodo para cambiar la palabra a adivinar
  String siguientePalabra();

  bool existePalabraEnElDiccionario(String palabra) =>
      palabras.contains(palabra);
}

class DiccionarioAleatorio extends Diccionario {
  DiccionarioAleatorio({required super.palabras});

  @override
  String siguientePalabra() {
    return palabras.elementAt(Random().nextInt(palabras.length));
  }
}

class DiccionarioSecuencial extends Diccionario {
  int palabraSeleccionada;

  DiccionarioSecuencial(
      {required super.palabras, required this.palabraSeleccionada});

  @override
  String siguientePalabra() {
    final siguientePalabra = palabraActual();
    palabraSeleccionada = (palabraSeleccionada + 1) % palabras.length;
    return siguientePalabra;
  }

  String palabraActual() {
    return palabras.elementAt(palabraSeleccionada);
  }
}
