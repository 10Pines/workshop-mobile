import 'package:flutter/material.dart';
import 'package:wordle/constantes/colores.dart';

class InformacionDeTecla {
  final String letra;
  Color? color;

  InformacionDeTecla({required this.letra});

  bool estaPintadaDeVerde() {
    return color == Colores.verde;
  }
}