import 'package:flutter/material.dart';
import 'package:wordle/modelo/teclado.dart';

///CampoDeLetra representa el ingreso de una letra
///Se encarga de manejar su controller, su focusNode y su color
class CampoDeLetra {
  final TextEditingController controlDeTexto = TextEditingController();
  final FocusNode focusNode = FocusNode();
  Color? color;
  final int posicion;

  CampoDeLetra({required this.posicion});

  /// Si corresponde setea [color] con [Colores.verde]. Pinta de [Colores.verde]
  /// la letra del teclado si pertenece a la palabra y está ubicada correctamente.
  String pintarVerde(String palabraDelDia, Teclado teclado) {
    //TODO: implementar utilizando [Teclado.pintar], los métodos [_noEstaPintado], [_existeLetraEn] y [_coincideLetraEnPosicion] pueden ayudar.
    return palabraDelDia;
  }

  /// Si corresponde setea [color] con [Colores.amarillo] o [Colores.gris].
  /// Pinta de [Colores.amarillo] la letra del teclado si pertenece a la palabra
  /// y no está ubicada correctamente. Pinta de [Colores.gris] si la letra no
  /// pertenece a la palabra
  String pintarAmarilloOGris(String palabraDelDia, Teclado teclado) {
    //TODO: implementar utilizando [Teclado.pintar], los métodos [_noEstaPintado], [_existeLetraEn] y [_coincideLetraEnPosicion] pueden ayudar.
    return palabraDelDia;
  }

  void escribirLetra(String letra, BuildContext context) {
    controlDeTexto.text = letra;
    if (posicion < 4) {
      FocusScope.of(context).nextFocus();
    }
  }

  void borrarLetra(BuildContext context) {
    if (controlDeTexto.text.isNotEmpty) {
      controlDeTexto.text = "";
    } else if (_noEsElPrimerCampo()) {
      FocusScope.of(context).previousFocus();
    }
  }

  void dispose() {
    controlDeTexto.dispose();
    focusNode.dispose();
  }

  bool tieneFocus() {
    return focusNode.hasFocus;
  }

  bool _noEsElPrimerCampo() => posicion != 0;

  bool _noEstaPintado() => color == null;

  bool _existeLetraEn(String palabraDelDia) =>
      palabraDelDia.contains(controlDeTexto.text);

  bool _coincideLetraEnPosicion(String palabraDelDia) {
    return posicion >= 0 &&
        posicion < palabraDelDia.length &&
        palabraDelDia[posicion] == controlDeTexto.text;
  }
}
