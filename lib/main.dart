import 'package:flutter/material.dart';
import 'package:wordle/modelo/diccionario.dart';
import 'package:wordle/screens/home_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    final Diccionario diccionario = DiccionarioAleatorio(
        palabras: ["HOLAS", "MANTO", "GATOS", "LIMON", "BELLA"]);
    return MaterialApp(
      title: 'Wordle',
      theme: ThemeData(
        inputDecorationTheme: const InputDecorationTheme(
          disabledBorder: OutlineInputBorder(),
          border: OutlineInputBorder(),
        ),
        elevatedButtonTheme: const ElevatedButtonThemeData(
          style: ButtonStyle(
            backgroundColor: MaterialStatePropertyAll(Colors.white60),
            foregroundColor: MaterialStatePropertyAll(Colors.black),
            minimumSize: MaterialStatePropertyAll(Size(8, 40)),
            padding: MaterialStatePropertyAll(EdgeInsets.symmetric(horizontal: 16)),
            shape: MaterialStatePropertyAll(RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(6)))),
          ),
        ),
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
        textTheme:
            const TextTheme(titleLarge: TextStyle(fontWeight: FontWeight.w900)),
        useMaterial3: true,
      ),
      home: HomePage(diccionario: diccionario),
    );
  }
}
