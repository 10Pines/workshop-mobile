import 'package:flutter/material.dart';
import 'package:wordle/constantes/reglas.dart';
import 'package:wordle/modelo/diccionario.dart';
import 'package:wordle/modelo/intento.dart';
import 'package:wordle/modelo/teclado.dart';
import 'package:wordle/widgets/tablero.dart';
import 'package:wordle/widgets/vista_de_teclado.dart';

class HomePage extends StatefulWidget {
  final Diccionario diccionario;

  const HomePage({
    super.key,
    required this.diccionario,
  });

  @override
  State<HomePage> createState() => _HomePageState();
}

/// Widgets que podemos utilizar y que utilizamos:
/// Column: representa una columna de widgets
/// Row: representa una fila de widgets
/// Padding: da separacion por fuera del widget que envuelve
/// TextField: contiene la logica para ingresar un texto.
///  -> Necesita un controller para manejar el texto ingresado
///  -> FocusNode para el manejo de foco
///  -> keyboardType: TextInputType.none para desactivar el uso del teclado del celular
///  -> OutlineInputBorder: para dar estilo al input con bordes en cada lado
/// AbsorbPointer: Anula la interaccion con los gestos en el widget que esta envolviendo
/// Expanded: expande un widget en el tamaño disponible.
/// Spacer: ocupa el tamaño disponible

//TODO: Completar Widget con manejo de estado
/// Metodos para manejar el estado: setState, initState
class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  bool terminoElJuego = false;
  int indiceIntentoActual = 0;
  ///Representa cada intento que puede realizar el jugador para ingresar una palabra
  late List<Intento> intentos;
  Teclado teclado = Teclado();

  Diccionario get diccionario => widget.diccionario;

  @override
  void initState() {
    intentos = crearIntentos();
    super.initState();
  }

  List<Intento> crearIntentos() {
    return List.generate(Reglas.intentosPosibles, (numeroDeIntento) {
      AnimationController controlDeAnimacion = AnimationController(
          vsync: this, duration: const Duration(milliseconds: 500));
      return Intento(numeroDeIntento, controlDeAnimacion: controlDeAnimacion);
    });
  }

  @override
  void dispose() {
    for (var intento in intentos) {
      intento.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text("Wordle"),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              Tablero(
                  intentos: intentos, indiceIntentoActual: indiceIntentoActual),
              const Spacer(),
              VistaDeTeclado(
                intento: intentos[indiceIntentoActual],
                teclado: teclado,
                terminoElJuego: terminoElJuego,
                comenzarNuevoJuego: comenzarNuevoJuego,
                chequearPalabra: chequearPalabra,
              )
            ],
          ),
        ),
      ),
    );
  }

  void chequearPalabra(BuildContext context) {
    //TODO: completar chequeo de la palabra utilizando los metodos del intento actual
    /// En [Mensaje] hay constantes definidas para mostrar el resultado del chequeo de la palabra.
    /// Luego del chequeo deben pintarse las celdas de los colores indicados.
  }

  void comenzarNuevoJuego() {
    //TODO: completar
    ///Reinicia la partida con una nueva palabra
  }
}
